
Sprite
------

This module provides an API to consolidate images into a single sprite image.

When an image hits the sprite registry, it will be entered into a static array.
At the end of a page load, all so entered images will be consolidated into a
single image made up of them all, and a CSS file will be generated to properly
display the image. The registry array will be saved to the cache with its md5
hash, so that future sprite images of the same composition may be retrieved
without rebuilding that image.

Two related projects, CSS Sprites and CSS Sprite Generator, work with themes,
and purport to work on the theme level, replacing theme css files with the
sprite files. This is meant to work in conjuction with that, but using page
generated images, such as image fields. In the future, as this module simply
provides an API, they may work together, ultimately creating one single image
and CSS file for all images and CSS background images on a page. For now, you
could, in theory, use the two modules together to create no more than two image
files for a single page.

To use it, simply call theme('sprite') in place of theme('image') when you'd
like images to be stored as CSS sprites. The rest is automatic.
