
2010-06-15
----------
  * Working dev version
  * Implemented a sprite image formatter
  * Implemented a sprite imagecache formatter
  * Implemented a register_sprite method
  * Flushed out the theme sprite method
  * Added an admin settings panel
  * Added an admin panel to flush generated sprites

2008-07
-------
  * Move _footer to _exit (aaron).
  * Check for sprite directory (aaron).
  * Create sprite css & image files (aaron).
  * Create theme function (aaron).
  * Create sprite registry (aaron).
  * Create install file (aaron).
  * Create initial module (aaron).
  * Create info file (aaron).
  * Create README.txt file (aaron).
